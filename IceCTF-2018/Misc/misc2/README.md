# Miscellaneous 2 - anticapcha

## Points
    - 250
---
## Description
>
    Wow, this is a big captcha. Who has enough time to solve this? Seems like a lot of effort to me!

    https://gg4ugw5xbsr2myw-anticaptcha.labs.icec.tf/
>
---
## Solve

The first step i did wass to have a look at what happens when i submit the form without any values. 

I then recieved the error message: `all questions wrong. 6 wrong.`

As there were more then 6 Questions in the form, I had the thought there must be 6 questions which had to
be answered.

After a while looking at the Questions, I saw that there were a few Questions which were not the same.

After answering those Questions I recieved the Flag.

The Flag is:
`IceCTF{ahh_we_have_been_captchured}`