# Cryptography 1 - Garfeld

## Points
    - 100
---
## Description
>
    You found the marketing campaign for a brand new sitcom. Garfeld! 
    It has a secret message engraved on it. Do you think you can figure out what they're trying to say?
>
---
## Solve



![IceCTF Crypto3](images/icectf2018/IceCrypto2.PNG "Crypto3")
 
 This is the text from the bottom of the image:
>
    IjgJUO{P_LOUV_AIRUS_GYQUTOLTD_SKRFB_TWNKCFT}
>

After thinking what type of Chipher this could be. i figured out it would be a `Shift Cipher`.

i used https://www.dcode.fr/vigenere-cipher to solve the challenge.

After entering the text into the `decoder` box, and then hitting decoder it didn'T give me the Flag.

I then realised that there was a number on the top of the Image. The numbers where `07271978`.

I then had a look how could i then make use of these numbers and saw that there was this fild `Knowing the Key:`.

I then transformed the Numbers to text: so 0 -> a , 1 -> b and so on. The numbers then become to `ahchbjhi`.

I inserted the Numbers in like this: `ahchbjhi`.

After then hitting the Decoder button i then recieved the Flag.

The Flag is:
`IceCTF{I_DONT_THINK_GRONSFELD_LIKES_MONDAYS}`
