# Forensics

***
* <a href="https://gitlab.com/snippets/1753982">Modern Picasso - solved by @Saiyanide
***
* <a href="https://gitlab.com/ketz/ctfs/blob/master/2018/icec.tf/hard-shells.md">Hard Shells - solved by @ketz
***
* <a href="https://gitlab.com/ketz/ctfs/blob/master/2018/icec.tf/lost-in-the-forest.md">Lost in the Forrest - solved by @ketz
![IceCTF Forensics](images/icectf2018/IceCtfForensic.PNG "Forensics")
