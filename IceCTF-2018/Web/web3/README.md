# Web 3 - Friðfinnur

## Points
    - 200
---
## Description
>
    Eve wants to make the hottest new website for job searching on the market! An avid PHP developer she decided to use the hottest new framework, Laravel! I don’t think she knew how to deploy websites at this scale        however….
>
---
## Solve

In the third web Challenge we were given a webiste which is used to list different available jobs on the market. 

Index.html:
![IceCTF index.html](images/icectf2018/Iceweb3a.PNG "Web3")

Jobs.html:
![IceCTF Jobs.html](images/icectf2018/Iceweb3b.PNG "Web3")

As I have been told it was Laravel framework, I did a bit of reaserch regarding the PHPDebuger which was active.

After digging arround abit i found out that there is a know issue with it called `whoops PHP errors for cool kids`. This known isseu is used for Degubbing where if there is an issue with a Link or the URL or even with the Code it will then redirect itself to the Debuger to be able to Identify what happend.

![IceCTF Debugger](images/icectf2018/Iceweb3c.PNG "Web3")

On that Page we then find the Flag:
`IceCTF{you_found_debug}`